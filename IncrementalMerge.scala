import java.sql.Date
import org.apache.spark.sql.types.{StructType, StructField, StringType, IntegerType, DoubleType, DateType}
import org.apache.spark.sql.{Row, DataFrame}

/**
  * Prepare Data
  */
def createDF(rows: Seq[Row], schema: StructType): DataFrame = {
  spark.createDataFrame(
    sc.parallelize(rows),
    schema
  )
}

val schema = StructType(
  List(
    StructField("order_no", StringType, true),
    StructField("customer_id", StringType, true),
    StructField("quantity", IntegerType, true),
    StructField("cost", DoubleType, true),
    StructField("order_date", DateType, true),
    StructField("last_updated_date", DateType, true)
  )
)

// Create orders dataframe
val orders = Seq(
  Row("001", "u1", 1, 15.00, Date.valueOf("2020-03-01"), Date.valueOf("2020-03-01")),
  Row("002", "u2", 1, 30.00, Date.valueOf("2020-04-01"), Date.valueOf("2020-04-01"))
)
val ordersDF = createDF(orders, schema)

// Create order_updates dataframe
val orderUpdates = Seq(
  Row("002", "u2", 1, 20.00, Date.valueOf("2020-04-01"), Date.valueOf("2020-04-02"))
)
val orderUpdatesDF = createDF(orderUpdates, schema)

ordersDF.show()
orderUpdatesDF.show()

/**
  * Incremental merge: SQL query
  */
ordersDF.createOrReplaceTempView("orders")
orderUpdatesDF.createOrReplaceTempView("order_updates")

val orderReconciledDF = spark.sql(
  """
  |SELECT unioned.*
  |FROM (
  |  SELECT * FROM orders x
  |  UNION ALL
  |  SELECT * FROM order_updates y
  |) unioned
  |JOIN
  |(
  |  SELECT
  |    order_no, customer_id,
  |    max(last_updated_date) as max_date
  |  FROM (
  |    SELECT * FROM orders
  |    UNION ALL
  |    SELECT * FROM order_updates
  |  ) t
  |  GROUP BY
  |    order_no, customer_id
  |) grouped
  |ON
  |  unioned.order_no = grouped.order_no AND
  |  unioned.customer_id = grouped.customer_id AND
  |  unioned.last_updated_date = grouped.max_date
  """.stripMargin
)

orderReconciledDF.show()

/**
  * Incremental merge: DataFrame API
  */
val keys = Seq("order_no", "customer_id")
val timestampCol = "last_updated_date"

val keysColumns = keys.map(ordersDF(_))

val unioned = ordersDF.union(orderUpdatesDF)

val grouped = unioned
  .groupBy(keysColumns: _*)
  .agg(
    max(timestampCol).as(timestampCol)
  )

val reconciled = grouped
  .join(unioned, keys :+ timestampCol)

reconciled.show()
