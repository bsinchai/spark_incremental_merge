# Incremental Merge with Spark #

Code samples from the [Incremental Merge with Apache Spark](https://www.phdata.io/blog/) blog.

### How to run ###

HQL:
```
hive -f incremental_merge.hql --hiveconf DATABASE=some_db;
```

Spark:
```
spark-shell -i incrementalMerge.scala
```
