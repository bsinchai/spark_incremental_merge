------------------------------------------------------------
--
-- Incremental merge code sample
--
-- Usage:
--    hive -f incremental_merge.hql \
--      --hiveconf DATABASE=DATABASE_NAME
--
------------------------------------------------------------

CREATE DATABASE IF NOT EXISTS ${hiveconf:DATABASE};
USE ${hiveconf:DATABASE};

------------------------------------------------------------
-- Create orders table
------------------------------------------------------------
DROP TABLE IF EXISTS ${hiveconf:DATABASE}.orders;

CREATE TABLE ${hiveconf:DATABASE}.orders (
    order_no STRING,
    customer_id STRING,
    quantity INT,
    cost DOUBLE,
    order_date DATE,
    last_updated_date DATE
);

INSERT INTO TABLE ${hiveconf:DATABASE}.orders VALUES
    ("001", "u1", 1, 15.00, "2020/03/01", "2020/03/01"),
    ("002", "u2", 1, 30.00, "2020/04/01", "2020/04/01");

SELECT * FROM ${hiveconf:DATABASE}.orders;

------------------------------------------------------------
-- Create order_updates table
------------------------------------------------------------
DROP TABLE IF EXISTS ${hiveconf:DATABASE}.order_updates;

CREATE TABLE ${hiveconf:DATABASE}.order_updates (
    order_no STRING,
    customer_id STRING,
    quantity INT,
    cost DOUBLE,
    order_date DATE,
    last_updated_date DATE
);

INSERT INTO TABLE ${hiveconf:DATABASE}.order_updates VALUES
    ("002", "u2", 1, 20.00, "2020/04/01", "2020/04/02");

SELECT * FROM ${hiveconf:DATABASE}.order_updates;

------------------------------------------------------------
-- Incremental merge and create order_reconciled view
------------------------------------------------------------
DROP VIEW IF EXISTS ${hiveconf:DATABASE}.order_reconciled;
CREATE VIEW ${hiveconf:DATABASE}.order_reconciled AS
SELECT unioned.*
FROM (
  SELECT * FROM orders x
  UNION ALL
  SELECT * FROM order_updates y
) unioned
JOIN
(
  SELECT
    order_no,
    customer_id,
    max(last_updated_date) as max_date
  FROM (
    SELECT * FROM orders
    UNION ALL
    SELECT * FROM order_updates
  ) t
  GROUP BY
    order_no, customer_id
) grouped
ON
  unioned.order_no = grouped.order_no AND
  unioned.customer_id = grouped.customer_id AND
  unioned.last_updated_date = grouped.max_date;

SELECT * FROM ${hiveconf:DATABASE}.order_reconciled;
